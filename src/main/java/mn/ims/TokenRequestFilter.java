package mn.ims;


import mn.ims.model.TokenAuthority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by ganbat on 2017-07-25.
 */
@Component
public class TokenRequestFilter extends OncePerRequestFilter {

    @Autowired
    TokenHelper tokenHelper;

    @Autowired
    TokenService tokenService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {


        String token = tokenHelper.getToken(request);
        Authentication authentication;

        if (token != null) {
            authentication = tokenService.getAuthentication(token);
            if (authentication == null) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
        } else {
            authentication = new AnonymousAuthenticationToken("ANONYMOUS", "ANONYMOUS", Arrays.asList(new TokenAuthority("ROLE_ANONYMOUSE")));
        }
        System.out.println("OK");
        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);

    }
}
