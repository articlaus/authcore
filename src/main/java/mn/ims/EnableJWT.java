package mn.ims;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Created by ganbat on 2017-07-25.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({JWTAuthentication.class})
public @interface EnableJWT {

}
