package mn.ims.model;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by ganbat on 2017-07-25.
 */
public class TokenAuthority implements GrantedAuthority {

    private String authority;

    public TokenAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return authority;
    }
}
