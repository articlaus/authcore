package mn.ims;

import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by ganbat on 2017-07-25.
 */
@Component
public class TokenHelper {

    private static final String TOKEN = "token";

    public String getToken(HttpServletRequest request) {
        return request.getHeader(TOKEN);
    }
}
