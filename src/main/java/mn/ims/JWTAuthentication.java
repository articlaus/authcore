package mn.ims;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

/**
 * Created by ganbat on 2017-07-25.
 */
@Configuration
@ComponentScan("mn.ims")
@EntityScan("mn.ims.postgres.entity")
@EnableJpaRepositories("mn.ims.postgres.dao")
@EnableGlobalMethodSecurity(prePostEnabled = true)
class JWTAuthentication extends WebSecurityConfigurerAdapter {

    @Autowired
    TokenRequestFilter tokenRequestFilter;

    @Autowired
    RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @Autowired
    JWTAuthenticationProvider jwtAuthenticationProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf()
                .disable()
                .authenticationProvider(jwtAuthenticationProvider)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint).and()
                .addFilterBefore(tokenRequestFilter, BasicAuthenticationFilter.class)
                /*.authorizeRequests()
                .anyRequest()
                .authenticated()*/;
    }
}
