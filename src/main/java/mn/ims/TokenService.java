package mn.ims;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.NonNull;
import lombok.extern.log4j.Log4j;
import mn.ims.model.TokenAuthentication;
import mn.ims.model.TokenAuthority;
import mn.ims.postgres.dao.UserDao;
import mn.ims.postgres.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ganbat on 2017-07-25.
 */
@Service
@Log4j
public class TokenService {
    @Autowired
    TokenHelper tokenHelper;

    @Autowired
    UserDao userDao;

    public Authentication getAuthentication(@NonNull String encodedToken) {
        try {
            JWT jwt = JWT.decode(encodedToken);

            /*String tokenId = jwt.getId();
            Token token = tokenDAO.findById(tokenId);*/

            String subject = jwt.getSubject();
            String roles = jwt.getClaim("role").asString();
            User token = userDao.findByUsername(subject);

            log.info("subject : " + subject);
            log.info("token : " + token);
            log.info("token.getSign : " + token.getPassword());
            log.info("token.getUsername : " + token.getUsername());
            log.info("token.role :" + roles);

            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(token.getPassword())).build();
            DecodedJWT verifiedJWT = verifier.verify(encodedToken);
            List<GrantedAuthority> authorities = new ArrayList<>();
            verifiedJWT.getClaim("role").asList(String.class).forEach(role -> {
                authorities.add(new TokenAuthority(role));
            });
            String username = verifiedJWT.getSubject();
            TokenAuthentication authentication = new TokenAuthentication(username, username, authorities);

            return authentication;
        } catch (Exception e) {
            log.info("error", e);
//            if (!(e instanceof NullPointerException))
//                log.error(e.getMessage(), e);
        }
        return null;
    }
}
